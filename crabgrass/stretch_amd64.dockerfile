FROM 0xacab.org:4567/leap/docker/ruby:stretch_amd64

MAINTAINER Azul <azul@riseup.net>
LABEL Description="Ruby build tools and debian packages to build crabgrass etc." Vendor="Riseup Labs" Version="3.x"

ENV DEBIAN_FRONTEND noninteractive

# install leap_cli prerequisites, see https://0xacab.org/leap/leap_cli
# and tools needed for ci (moreutils and expect)
RUN apt-get update \
  && apt-get upgrade -y \
  && apt-get install -y --no-install-recommends \
    libsqlite3-dev \
    default-libmysqlclient-dev \
    graphicsmagick \
    inkscape \
    default-mysql-client \
    sphinxsearch \
    libreoffice-writer \
    file \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*
