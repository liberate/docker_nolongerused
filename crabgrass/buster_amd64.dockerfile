FROM 0xacab.org:4567/leap/docker/ruby:buster_amd64

MAINTAINER Azul <azul@riseup.net>
LABEL Description="Debian stable build with tools and packages for
crabgrass" Vendor="Riseup Labs" Version="3.x"

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update \
  && apt-get upgrade -y \
  && apt-get install -y --no-install-recommends \
    libsqlite3-dev \
    default-libmysqlclient-dev \
    graphicsmagick \
    inkscape \
    default-mysql-client \
    sphinxsearch \
    libreoffice-writer \
    file \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*
