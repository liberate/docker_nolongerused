#!/bin/bash
#
# Build script for the gitlab ci.
#
# usage: build.sh DIR [TAG]
#
# Will run docker build in DIR with DIR/Dockerfile or
# DIR/TAG.dockerfile if TAG is given.
#
# Assumes CI specific environment variables to be set:
# CI_REGISTRY_IMAGE
#

DIR=$1
TAG=${2:-latest}
DOCKERFILE=$DIR/${2:-Dockerfile}${2:+.dockerfile}
TARGET=$CI_REGISTRY_IMAGE/$DIR:$TAG

docker login -u gitlab-ci-token -e sysdev@leap.se -p "$CI_JOB_TOKEN" "$CI_REGISTRY"
docker build $DOCKER_BUILD_OPTS -t "$TARGET" -f "$DOCKERFILE" "$DIR"
docker push "$TARGET"
