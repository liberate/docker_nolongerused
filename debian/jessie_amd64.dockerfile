FROM debian:jessie-backports

MAINTAINER LEAP Encryption Access Project <sysdev@leap.se>
LABEL Description="Bare debian jessie baseimage with few customisation" Vendor="LEAP" Version="1.x"

ENV DEBIAN_FRONTEND noninteractive

RUN sed -i 's/httpredir/deb/' /etc/apt/sources.list && \
    sed -i 's/httpredir/deb/' /etc/apt/sources.list.d/backports.list

RUN apt-get update \
  && apt-get -y dist-upgrade \
  && apt-get install -y --no-install-recommends \
    git \
    leap-archive-keyring \
    locales \
    docker.io \
    curl \
    python \
    ca-certificates \
    sudo && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

RUN echo "deb http://deb.leap.se/testing-platform jessie main" > /etc/apt/sources.list.d/leap.list && \
    apt-get update

RUN localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8
ENV LANG en_US.utf8

RUN useradd -ms /bin/bash -G sudo cirunner
RUN adduser cirunner docker

# allow all members of sudo group to execute any command without password
RUN sed -i 's/^%sudo.*/%sudo ALL=(ALL) NOPASSWD:ALL/' /etc/sudoers

COPY files/etc /etc/
