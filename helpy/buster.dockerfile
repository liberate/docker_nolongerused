FROM ruby:2.5

MAINTAINER Azul <azul@riseup.net>
LABEL Description="Ruby 2.5 build tools and debian packages to test helpy" Vendor="Riseup Labs" Version="3.x"

ENV DEBIAN_FRONTEND noninteractive

# install helpy prerequisites,
# and tools needed for ci (nodejs and chromium)
RUN apt update \
  && apt dist-upgrade -y \
  && wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb \
  && apt install -y ./google-chrome-stable_current_amd64.deb \
  && apt install -y chromium-driver \
  && apt install -y nodejs \
  && apt install -y libsodium-dev \
  && gem install bundler -v '< 2' \
  && apt clean \
  && rm -rf /var/lib/apt/lists/*
